PATH_TO_SERVER=`pwd`
if [ ! -d "$PATH_TO_SERVER" ]; then
	echo "Error: No se encuentra instalación de servidor."
	exit -1
fi
#LOG_FILE=webserver.log
LOG_FILE=stdout
if [ -z "$LOG_FILE" ]; then
	LOG_FILE=/dev/null
elif [ "$LOG_FILE" == "stdout" ]; then
	LOG_FILE=/dev/stdout
else
	LOG_FILE=${PATH_TO_SERVER}/${LOG_FILE}
fi
BACKGROUND="N"
case "$1" in
    start)
	echo "Levantando webserver de Gluten Free NOA...">>${LOG_FILE}
	if [ "$BACKGROUND" == "Y" ];then
		python ${PATH_TO_SERVER}/manage.py runserver 0.0.0.0:8000 >> ${LOG_FILE} 2>&1 &
	elif [ "$BACKGROUND" == "N" ];then
		python ${PATH_TO_SERVER}/manage.py runserver 0.0.0.0:8000 >> ${LOG_FILE}
	fi
    ;;
    stop)
	echo "Matando webserver de Gluten Free NOA...">>${LOG_FILE}
	kill $(pgrep -f "^.*manage.py.*runserver.*$" ) 2>/dev/null
    ;;
    restart)
	echo "Relanzando webserver de Gluten Free NOA...">>${LOG_FILE}
        $0 stop && $0 start
    ;;
    status)
 	pgrep -f "^.*manage.py.*runserver.*$">/dev/null
	rsp=$?
	if [ $rsp -eq 0 ]; then
	    echo "Servidor arriba..."
	else
	    echo "Servidor caido..."
	fi
        exit $rsp
    ;;
    *)
        echo "Uso: /etc/init.d/glutenfreeNOA.sh {start|stop|status}" 
        exit 1
    ;;
esac
