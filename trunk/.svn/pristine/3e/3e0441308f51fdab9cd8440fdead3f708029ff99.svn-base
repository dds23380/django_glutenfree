# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from solo.models import SingletonModel
from Crypto.Random.random import StrongRandom
import base64
from funciones import redimensionar_imagen
from glutenfreeNOA.settings import *
from django.db.models import signals
from ckeditor.fields import RichTextField


# Create your models here.
class Agenda(models.Model):
	nombre = models.CharField(max_length=64)
	email = models.CharField(max_length=64,blank=True)
	fijo = models.CharField(max_length=64,blank=True)
	celular = models.CharField(max_length=64,blank=True)
	foto_perfil = models.ImageField(upload_to="foto_perfiles",blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def __str__(self):
		return self.nombre
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Agenda, self).save()
	def empaquetar(self):
		try:
			img=self.foto_perfil.url
		except:
			img=""
		rsp=str(self.id)+";"+self.nombre+";"+self.email+";"+self.fijo+";"+self.celular+";"+img+";"+self.str_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	
class Usuario(models.Model):
	usuario = models.CharField(max_length=64)
	contrasena =  models.CharField(max_length=64,blank=True)
	permisos = models.IntegerField(default=1000)
	agenda = models.ForeignKey('Agenda')
	def __str__(self):
		return self.usuario
	def empaquetar(self):
		try:
			id_agenda=str(self.agenda.id)
		except:
			id_agenda=""
		rsp=str(self.id)+";"+self.usuario +";"+self.contrasena +";"+str(self.permisos)+";"+id_agenda
		return rsp

class Comentario(models.Model):
	mensaje = models.TextField()
	usuario = models.ForeignKey('Usuario')
	fecha = models.DateTimeField(auto_now_add=True, blank=True)
	def __str__(self):
		return self.usuario.usuario+' -> '+self.mensaje[:20]

class Blog(models.Model):
	titulo = models.CharField(max_length=128)
	descripcion = RichTextField(config_name='awesome_ckeditor',blank=True)
	imagen = models.ImageField(upload_to="foto_articulos",blank=True)
	modo_lista=models.BooleanField(default=True)
	articulos = models.ManyToManyField('Articulo',blank=True)
	def __str__(self):
		return self.titulo
	def nextid(self,articulo_id):
		found=False
		if articulo_id == self.articulos.all().last().id:
			return ""
		else:
			for articulo in self.articulos.all():
				if found==True:
					return articulo.id
				if str(articulo.id)==articulo_id:
					found=True
			return ""
	def previousid(self,articulo_id):
		found=False
		if articulo_id == self.articulos.all().first().id:
			return ""
		else:
			for articulo in reversed(self.articulos.all()):
				if found==True:
					return articulo.id
				if str(articulo.id)==articulo_id:
					found=True
			return ""

class Articulo(models.Model):
	titulo = models.CharField(max_length=128)
	descripcion = models.CharField(max_length=256,blank=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)
	mensaje=RichTextField(config_name='awesome_ckeditor',blank=True)
	imagen = models.ImageField(upload_to="foto_articulos",blank=True)
	comentarios = models.ManyToManyField(Comentario)
	def __str__(self):
		return self.titulo
	def __unicode__(self):
		return self.titulo

class Informacion(models.Model):
	titulo = models.CharField(max_length=64)
	mensaje=RichTextField(config_name='awesome_ckeditor',blank=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True,editable=False)
	page_hash = models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def save(self):
		self.page_hash=str(StrongRandom().getrandbits(39))
		super(Informacion, self).save()
	def empaquetar(self):
		if self.mensaje:
			pagina_html=base64.standard_b64encode(self.mensaje)
		else:
			pagina_html=""
		fecha_f=str(self.fecha_creacion)[0:19]
		rsp=str(self.id)+";"+str(self.titulo)+";"+pagina_html+";"+fecha_f+";"+self.page_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.page_hash
		return rsp
	def __str__(self):
		return self.titulo
	def __unicode__(self):
		return self.titulo

class Tienda(models.Model):
	nombre = models.CharField(max_length=64)
	tipo =  models.IntegerField(default=1)
	def __str__(self):
		return self.nombre
	def empaquetar(self):
		return str(self.id)+";"+self.nombre+";"+str(self.tipo)

class Ciudad(models.Model):
	ciudad = models.CharField(max_length=64)
	provincia = models.CharField(max_length=64)
	pais = models.CharField(max_length=64)
	def __str__(self):
		return self.ciudad +" "+ self.provincia +" "+ self.pais
	def empaquetar(self):
		return str(self.id)+";"+self.ciudad +";"+ self.provincia +";"+ self.pais

class Voto(models.Model):
	voto = models.IntegerField()
	usuario=models.ForeignKey('Usuario')
	comentario =  models.CharField(max_length=128)
	def __str__(self):
		return self.usuario.usuario +" "+str(self.voto)
	def empaquetar(self):
		return str(self.id)+";"+str(self.voto)+";"+str(self.usuario.id)+";"+self.comentario

class Localizacion(models.Model):
	tienda=models.ForeignKey('Tienda')
	calle=models.CharField(max_length=64)
	altura=models.IntegerField()
	latitud=models.CharField(max_length=12)
	longitud=models.CharField(max_length=12)
	ciudad=models.ForeignKey('Ciudad')
	agendas = models.ManyToManyField(Agenda,blank=True)
	votos = models.ManyToManyField(Voto,blank=True)
	imagen = models.ImageField(upload_to="foto_localizaciones",blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def __str__(self):
		return self.tienda.nombre +" "+ self.calle
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Localizacion, self).save()
	def empaquetar(self):
		id_agendas=""
		for agenda in self.agendas.all():
			if id_agendas=="":
				id_agendas=str(agenda.id)
			else:
				id_agendas=id_agendas+","+str(agenda.id)
		id_votos=""
		for voto in self.votos.all():
			if id_votos=="":
				id_votos=str(voto.id)
			else:
				id_votos=id_votos+","+str(voto.id)
		try:
			img=self.imagen.url
		except:
			img=""
		rsp=str(self.id)+";"+str(self.tienda.id)+";"+self.calle+";"+str(self.altura)+";"+self.latitud+";"+self.longitud+";"+str(self.ciudad.id)+";"+id_agendas+";"+id_votos+";"+img+";"+self.str_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp

def post_save_localizacion(sender, instance, created, **kwargs):
	if instance.imagen:
		redimensionar_imagen(BASE_DIR+instance.imagen.url,400)

signals.post_save.connect(post_save_localizacion, sender=Localizacion)

class Producto(models.Model):
	marca = models.CharField(max_length=128)
	denominacion = models.CharField(max_length=512)
	rnpa=models.CharField(max_length=32)
	fecha_baja = models.DateField(null=True,blank=True)
	motivo_baja = models.CharField(max_length=256,null=True,blank=True)
	def empaquetar(self):
		rsp=str(self.id)+";"+unicode(self.marca)+";"+unicode(self.denominacion)+";"+str(self.rnpa)+";"+str(self.fecha_baja)+";"+unicode(self.motivo_baja)
		return rsp
	def __str__(self):
		return self.marca+" "+self.rnpa
	def __unicode__(self):
		return self.marca+" "+self.rnpa

class Grupo(models.Model):
	tipo = models.CharField(max_length=256)
	productos = models.ManyToManyField(Producto,blank=True)
	str_hash= models.CharField(max_length=4,blank=True,default='0000', editable=False)
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(13))
		super(Grupo, self).save()
	def empaquetar(self):
		id_prod=""
		for producto in self.productos.all():
			if id_prod=="":
				id_prod=str(producto.id)
			else:
				id_prod=id_prod+","+str(producto.id)
		rsp=str(self.id)+";"+unicode(self.tipo)+";"+id_prod+";"+str(self.str_hash)
		return rsp
	def empaquetar_tipo(self):
		rsp=str(self.id)+";"+unicode(self.tipo)+";"+str(self.str_hash)
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	def __str__(self):
		return self.tipo
	def __unicode__(self):
		return self.tipo

class Listado_Alimento(SingletonModel):
	fecha_actualizacion = models.DateField(null=True,blank=True)
	proxima_actualizacion = models.DateField(null=True,blank=True)
	grupos = models.ManyToManyField(Grupo,blank=True)
	def __str__(self):
		return "LISTADO INTEGRADO DE ALIMENTOS LIBRES DE GLUTEN - LEY 26.588"
	class Meta:
		verbose_name = "LISTADO INTEGRADO DE ALIMENTOS LIBRES DE GLUTEN"
