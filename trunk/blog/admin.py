from django.contrib import admin
from .models import *


# Register your models here.

class ArticuloAdmin(admin.ModelAdmin):
	list_display = ('titulo','descripcion','fecha_creacion')
	fields = ['titulo','descripcion','mensaje','imagen','modo']
admin.site.register(Articulo,ArticuloAdmin)
class ArticuloUsuarioAdmin(admin.ModelAdmin):
	list_display = ('titulo','fecha_creacion')
	fields = ['titulo','mensaje']
admin.site.register(Articulo_Usuario,ArticuloUsuarioAdmin)
class BlogAdmin(admin.ModelAdmin):
	list_display = ('titulo','descripcion')
	fields = ['titulo','descripcion','imagen','articulos','modo','articulos_usuario']
admin.site.register(Blog,BlogAdmin)
admin.site.register(Informacion)
admin.site.register(Comentario)
