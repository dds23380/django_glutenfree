from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'detalle/(?P<blog_id>[0-9]+)-(?P<question_id>[0-9]+)/$', views.detalle),
    url(r'lista_detalle/(?P<question_id>[0-9]+)/$', views.lista_detalle),
    url(r'login/$', views.login),
    url(r'create_user/$', views.create_user),
    url(r'config_account/$', views.config_account),
    url(r'comentario/(?P<articulo_id>[0-9]+)/$', views.comentar),
    url(r'comentario_chat/(?P<articulo_id>[0-9]+)/$', views.comentar_chat),
    url(r'actualizar_comentarios/$', views.actualizar_comentarios),
    url(r'eliminar_comentario/(?P<articulo_id>[0-9]+)_(?P<comentario_id>[0-9]+)/$', views.borrar_comentario),
    url(r'logout/$', views.logout),
    url(r'eliminar_cuenta/$', views.eliminar_cuenta),
]


