# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='articulo',
            options={'verbose_name': 'Art\xedculo', 'verbose_name_plural': 'Listado de art\xedculos disponibles asociados a los blogs'},
        ),
        migrations.AlterModelOptions(
            name='blog',
            options={'verbose_name': 'Blog', 'verbose_name_plural': 'Listado de los blogs'},
        ),
        migrations.AlterModelOptions(
            name='comentario',
            options={'verbose_name': 'Comentario', 'verbose_name_plural': 'Listado general de comentarios en el blog'},
        ),
        migrations.AlterModelOptions(
            name='informacion',
            options={'verbose_name': 'P\xe1gina', 'verbose_name_plural': 'Informaci\xf3n \xfatil para cel\xedacos'},
        ),
    ]
