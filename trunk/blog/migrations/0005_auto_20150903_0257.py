# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_articulo_modo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comentario',
            name='mensaje',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
