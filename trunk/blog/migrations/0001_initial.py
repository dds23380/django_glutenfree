# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('movil_server', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Articulo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=128)),
                ('descripcion', models.CharField(max_length=256, blank=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('mensaje', ckeditor.fields.RichTextField(blank=True)),
                ('imagen', models.ImageField(upload_to='foto_articulos', blank=True)),
            ],
            options={
                'verbose_name': 'Listado de art\xedculos disponibles para el blog',
            },
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=128)),
                ('descripcion', ckeditor.fields.RichTextField(blank=True)),
                ('imagen', models.ImageField(upload_to='foto_articulos', blank=True)),
                ('modo_lista', models.BooleanField(default=True)),
                ('articulos', models.ManyToManyField(to='blog.Articulo', blank=True)),
            ],
            options={
                'verbose_name': 'Blog donde se arman los distintos Post',
            },
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mensaje', models.TextField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('usuario', models.ForeignKey(to='movil_server.Usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Informacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=64)),
                ('mensaje', ckeditor.fields.RichTextField(blank=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('page_hash', models.CharField(default='000000000000', max_length=12, editable=False, blank=True)),
            ],
            options={
                'verbose_name': 'Informaci\xf3n \xfatil para cel\xedaco',
            },
        ),
        migrations.AddField(
            model_name='articulo',
            name='comentarios',
            field=models.ManyToManyField(to='blog.Comentario'),
        ),
    ]
