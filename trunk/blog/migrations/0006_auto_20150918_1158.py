# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150903_0257'),
    ]

    operations = [
        migrations.CreateModel(
            name='Articulo_Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=128)),
                ('mensaje', ckeditor.fields.RichTextField(blank=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('comentarios', models.ManyToManyField(to='blog.Comentario')),
            ],
            options={
                'verbose_name': 'Art\xedculo del usuario',
                'verbose_name_plural': 'Listado de art\xedculos disponibles realizados por los usuarios',
            },
        ),
        migrations.AddField(
            model_name='blog',
            name='articulos_usuario',
            field=models.ManyToManyField(to='blog.Articulo_Usuario', blank=True),
        ),
    ]
