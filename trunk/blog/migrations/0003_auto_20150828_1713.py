# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20150828_1441'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blog',
            name='modo_lista',
        ),
        migrations.AddField(
            model_name='blog',
            name='modo',
            field=models.IntegerField(default=1, choices=[(1, 'MODO LISTA, EL USUARIO PUEDE AGREGAR ARTICULOS'), (2, 'MODO LISTA, EL USUARIO NO PUEDE AGREGAR ARTICULOS'), (3, 'MODO DETALLE')]),
        ),
    ]
