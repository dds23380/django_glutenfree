# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movil_server', '0004_auto_20150916_1700'),
        ('blog', '0006_auto_20150918_1158'),
    ]

    operations = [
        migrations.AddField(
            model_name='articulo_usuario',
            name='autor',
            field=models.ForeignKey(to='movil_server.Usuario', null=True),
        ),
    ]
