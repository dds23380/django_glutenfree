# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20150828_1713'),
    ]

    operations = [
        migrations.AddField(
            model_name='articulo',
            name='modo',
            field=models.IntegerField(default=1, choices=[(1, 'FORO'), (2, 'CHAT')]),
        ),
    ]
