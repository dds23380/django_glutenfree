# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from Crypto.Random.random import StrongRandom
import base64
from glutenfreeNOA.settings import *
from ckeditor.fields import RichTextField
from movil_server.models import Usuario

MODO_CHOICES = ((1, 'MODO LISTA, EL USUARIO PUEDE AGREGAR ARTICULOS'),(2, 'MODO LISTA, EL USUARIO NO PUEDE AGREGAR ARTICULOS'),(3, 'MODO DETALLE'))
MODO_CHAT_CHOICES = ((1, 'FORO'),(2, 'CHAT'))

class Comentario(models.Model):
	mensaje = RichTextField(config_name='awesome_ckeditor')
	usuario = models.ForeignKey(Usuario)
	fecha = models.DateTimeField(auto_now_add=True, blank=True)
	def __str__(self):
		return self.usuario.usuario +'('+str(self.fecha)[:19]+') -> '+self.mensaje[:30]
	def __unicode__(self):
		return self.usuario.usuario +'('+str(self.fecha)[:19]+') -> '+self.mensaje[:30]
	def empaquetar(self):
		return str(self.id)+";"+base64.standard_b64encode(self.mensaje.encode("utf-8"))+";"+str(self.usuario.id)+";"+str(self.fecha)
	class Meta:
		verbose_name = "Comentario"
		verbose_name_plural = "Listado general de comentarios en el blog"

class Blog(models.Model):
	titulo = models.CharField(max_length=128)
	descripcion = RichTextField(config_name='awesome_ckeditor',blank=True)
	imagen = models.ImageField(upload_to="foto_articulos",blank=True)
	modo=models.IntegerField(choices=MODO_CHOICES,default=1)
	articulos = models.ManyToManyField('Articulo',blank=True)
	articulos_usuario = models.ManyToManyField('Articulo_Usuario',blank=True)
	def __str__(self):
		return self.titulo
	def nextid(self,articulo_id):
		found=False
		if articulo_id == self.articulos.all().last().id:
			return ""
		else:
			for articulo in self.articulos.all():
				if found==True:
					return articulo.id
				if str(articulo.id)==articulo_id:
					found=True
			return ""
	def previousid(self,articulo_id):
		found=False
		if articulo_id == self.articulos.all().first().id:
			return ""
		else:
			for articulo in reversed(self.articulos.all()):
				if found==True:
					return articulo.id
				if str(articulo.id)==articulo_id:
					found=True
			return ""
	class Meta:
		verbose_name = "Blog"
		verbose_name_plural = "Listado de los blogs"

class Articulo(models.Model):
	titulo = models.CharField(max_length=128)
	descripcion = models.CharField(max_length=256,blank=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)
	mensaje=RichTextField(config_name='awesome_ckeditor',blank=True)
	imagen = models.ImageField(upload_to="foto_articulos",blank=True)
	comentarios = models.ManyToManyField(Comentario)
	modo=models.IntegerField(choices=MODO_CHAT_CHOICES,default=1)
	def __str__(self):
		return self.titulo
	def __unicode__(self):
		return self.titulo
	class Meta:
		verbose_name = "Artículo"
		verbose_name_plural = "Listado de artículos disponibles asociados a los blogs"

class Articulo_Usuario(models.Model):
	titulo = models.CharField(max_length=128)
	mensaje=RichTextField(config_name='awesome_ckeditor',blank=True)
	comentarios = models.ManyToManyField(Comentario)
	fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)
	autor = models.ForeignKey(Usuario,null=True)
	def __str__(self):
		return self.titulo
	def __unicode__(self):
		return self.titulo
	class Meta:
		verbose_name = "Artículo del usuario"
		verbose_name_plural = "Listado de artículos disponibles realizados por los usuarios"

class Informacion(models.Model):
	titulo = models.CharField(max_length=64)
	mensaje=RichTextField(config_name='awesome_ckeditor',blank=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True,editable=False)
	page_hash = models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def save(self):
		self.page_hash=str(StrongRandom().getrandbits(39))
		super(Informacion, self).save()
	def empaquetar(self):
		if self.mensaje:
			pagina_html=base64.standard_b64encode(self.mensaje)
		else:
			pagina_html=""
		fecha_f=str(self.fecha_creacion)[0:19]
		rsp=str(self.id)+";"+str(self.titulo)+";"+pagina_html+";"+fecha_f+";"+self.page_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.page_hash
		return rsp
	def __str__(self):
		return self.titulo
	def __unicode__(self):
		return self.titulo
	class Meta:
		verbose_name = "Página"
		verbose_name_plural = "Información útil para celíacos"
