# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models import signals
from Crypto.Random.random import StrongRandom
from solo.models import SingletonModel
import base64
from funciones import redimensionar_imagen
from glutenfreeNOA.settings import *

# Create your models here.

TIPO_ESPECIALIDAD_CHOICES = ((1, 'MEDICO'),(2, 'KINESIOLOGIA'),(3, 'ENFERMERIA'),(4, 'FONOAUDIOLIGIA'),(5, 'NUTRICION'))

ESPECIALIDAD_CHOICES = ((1, 'ALERGOLOGIA'),(2, 'ANATOMIA PATOLOGICA'),(3, 'ANESTESIOLOGIA'),(4, 'BACTERIOLOGIA MEDICA')
,(5, 'CARDIOLOGIA'),(6, 'CARDIOLOGIA INFANTIL'),(7, 'CIRUGIA CARDIOVASCULAR'),(8, 'CIRUGIA DE CABEZA Y CUELLO')
,(9, 'CIRUGIA DE TORAX'),(10, 'CIRUGIA GENERAL'),(11, 'CIRUGIA PEDIATRICA'),(12, 'CIRUGIA PLASTICA')
,(13, 'CLINICA MEDICA'),(14, 'DERMATOLOGIA'),(15, 'DIAGNOSTICO POR IMAGENES'),(16, 'ECOGRAFIA')
,(17, 'ENDOCRINOLOGIA'),(18, 'FISIATRIA'),(19, 'FLEBOLOGIA Y LINFOLOGIA'),(20, 'GASTROENTEROLOGIA')
,(21, 'GENETICA'),(22, 'GERIATRIA'),(23, 'GINECOLOGIA'),(24, 'HEBIATRIA')
,(25, 'HEMATO-ONCOLOGIA PEDIATRICA'),(26, 'HEMATOLOGIA'),(27, 'INFECTOLOGIA'),(28, 'MEDICINA DE FAMILIA')
,(29, 'MEDICINA DEL DEPORTE'),(30, 'MEDICINA DEL TRABAJO'),(31, 'MEDICINA GENERAL'),(32, 'MEDICINA NUCLEAR')
,(33, 'MEDICO'),(34, 'MEDICO GENERALISTA'),(35, 'Muchas Especialidades'),(36, 'NEFROLOGIA')
,(37, 'NEONATOLOGIA'),(38, 'NEUMONOLOGIA'),(39, 'NEUMONOLOGIA INFANTIL'),(40, 'NEUROCIRUGIA')
,(41, 'NEUROLOGIA'),(42, 'NEUROLOGIA INFANTIL'),(43, 'NUTRICION'),(44, 'OFTALMOLOGIA')
,(45, 'ONCOLOGIA'),(46, 'ORTOPEDIA Y TRAUMATOLOGIA'),(47, 'ORTOPEDIA Y TRAUMATOLOGIA INFANTIL'),(48, 'OTORRINOLARINGOLOGIA')
,(49, 'PEDIATRIA'),(50, 'PSIQUIATRIA'),(51, 'RADIOLOGIA'),(52, 'REUMATOLOGIA')
,(53, 'TERAPIA INTENSIVA'),(54, 'TOCOGINECOLOGIA'),(55, 'UROLOGIA'),(56, 'LICENCIATURA'))

TIENDA_CHOICES = ((1, 'Dietética'),(2, 'Grandes tiendas'),(3, 'Restaurante & Bar'))

# Create your models here.
class Agenda(models.Model):
	nombre = models.CharField(max_length=64)
	email = models.CharField(max_length=64,default='NOEMAIL')
	fijo = models.CharField(max_length=64,blank=True)
	celular = models.CharField(max_length=64,blank=True)
	foto_perfil = models.ImageField(upload_to="foto_perfiles",blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def __str__(self):
		return self.nombre
	def __unicode__(self):
		return unicode(self.nombre)
	def save(self,force_insert=False, using=None):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Agenda, self).save()
	def empaquetar(self):
		try:
			img=self.foto_perfil.url
		except:
			img=""
		rsp=str(self.id)+";"+self.nombre+";"+self.email+";"+self.fijo+";"+self.celular+";"+img+";"+self.str_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	class Meta:
		verbose_name = "Agenda"
		verbose_name_plural = "Lista de agendas asociada al usuario o comercio"

class Medico(models.Model):
	nombre = models.CharField(max_length=64)
	matricula = models.CharField(max_length=64,blank=True)
	especialidad = models.ManyToManyField('Especialidad')
	foto_perfil = models.ImageField(upload_to="foto_perfiles_medicos",blank=True)
	atencion = models.ManyToManyField('Atencion',blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Medico, self).save()
	def __str__(self):
		return self.nombre
	def __unicode__(self):
		return self.nombre
	def get_hash(self):
		return self.str_hash
	def empaquetar(self):
		id_esp=""
		for esp in self.especialidad.all():
			if id_esp=="":
				id_esp=str(esp.tipo)+" "+str(esp.especialidad)+","
			else:
				id_esp=id_esp+str(esp.tipo)+" "+str(esp.especialidad)+","
		try:
			img=self.foto_perfil.url
		except:
			img=""
		id_atencion=""
		for atencion in self.atencion.all():
			if id_atencion=="":
				id_atencion=str(atencion.id)
			else:
				id_atencion=id_atencion+","+str(atencion.id)
		try:
			rsp=str(self.id)+";"+unicode(self.nombre)+";"+str(self.matricula)+";"+str(id_esp)+";"+str(img)+";"+str(id_atencion)+";"+str(self.str_hash)
		except:
			rsp=""
		return rsp
	class Meta:
		verbose_name = "Médico"
		verbose_name_plural = "Listado de Médicos"

class Especialidad(models.Model):
	tipo = models.IntegerField(choices=TIPO_ESPECIALIDAD_CHOICES,default=1)
	especialidad = models.IntegerField(choices=ESPECIALIDAD_CHOICES,default=20)
	def __str__(self):
		return TIPO_ESPECIALIDAD_CHOICES[self.tipo-1][1]+ ": "+ESPECIALIDAD_CHOICES[self.especialidad-1][1]
	def save(self):
		if self.tipo==1:
			if self.especialidad==56:
				return
		else:
			self.especialidad=56
		super(Especialidad, self).save()
	class Meta:
		verbose_name = "Especialidad médica"
		verbose_name_plural = "Especialidades médicas"

class Atencion (models.Model):
	tipo = models.CharField(max_length=64)
	domicilio = models.CharField(max_length=64)
	ciudad=models.ForeignKey('Ciudad')
	latitud=models.CharField(max_length=12,blank=True)
	longitud=models.CharField(max_length=12,blank=True)
	horario = models.CharField(max_length=64,blank=True)
	agenda = models.ManyToManyField('Agenda',blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Atencion, self).save()
	def __str__(self):
		return self.tipo+" - "+self.domicilio+" "+str(self.ciudad)+" - "+self.horario
	def __unicode__(self):
		return self.tipo+" - "+self.domicilio+" "+str(self.ciudad)+" - "+self.horario
	def empaquetar(self):
		id_agenda=""
		for agenda in self.agenda.all():
			if id_agenda=="":
				id_agenda=str(agenda.id)
			else:
				id_agenda=id_agenda+","+str(agenda.id)
		rsp=str(self.id)+";"+self.tipo+";"+self.domicilio+";"+str(self.ciudad.id)+";"+self.latitud+";"+self.longitud+";"+self.horario+";"+id_agenda+";"+self.str_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	class Meta:
		verbose_name = "Consultorio"

class Usuario(models.Model):
	usuario = models.CharField(max_length=64)
	contrasena =  models.CharField(max_length=64,blank=True)
	permisos = models.IntegerField(default=1000)
	agenda = models.ForeignKey('Agenda')
	def __str__(self):
		return self.usuario
	def __unicode__(self):
		return unicode(self.usuario)
	def empaquetar(self):
		try:
			id_agenda=str(self.agenda.id)
		except:
			id_agenda=""
		rsp=str(self.id)+";"+self.usuario +";"+id_agenda
		return rsp

class Tienda(models.Model):
	nombre = models.CharField(max_length=64)
	tipo =  models.IntegerField(choices=TIENDA_CHOICES,default=1)
	def __str__(self):
		return self.nombre
	def __unicode__(self):
		return unicode(self.nombre)
	def empaquetar(self):
		return str(self.id)+";"+self.nombre+";"+str(self.tipo)
	class Meta:
		verbose_name = "tienda"
		verbose_name_plural = "Definición de tiendas"

class Ciudad(models.Model):
	ciudad = models.CharField(max_length=64)
	provincia = models.CharField(max_length=64)
	pais = models.CharField(max_length=64)
	def __str__(self):
		return self.ciudad +" "+ self.provincia +" - "+ self.pais
	def __unicode__(self):
		return self.ciudad +" "+ self.provincia +" "+ self.pais
	def empaquetar(self):
		return str(self.id)+";"+self.ciudad +";"+ self.provincia +";"+ self.pais
	class Meta:
		verbose_name = "Ciudad"
		verbose_name_plural = "Ciudades"

class Voto(models.Model):
	voto = models.IntegerField()
	usuario=models.ForeignKey('Usuario')
	comentario =  models.TextField(max_length=128,blank=True)
	def save(self):
		voto=self.voto
		try:
			if voto <1 or voto >3:
				self.voto=2
		except:
			self.voto=2
		super(Voto, self).save()
	def __str__(self):
		return str(self.usuario) +" -> "+self.comentario
	def __unicode__(self):
		return unicode(self.usuario) +" -> "+unicode(self.comentario)
	def empaquetar(self):
		return str(self.id)+";"+str(self.voto)+";"+str(self.usuario.id)+";"+base64.standard_b64encode(self.comentario)
	class Meta:
		verbose_name = "Valoración del usuario"
		verbose_name_plural = "Listado general de valoración del usuario sobre los comercios"

class Localizacion(models.Model):
	tienda=models.ForeignKey('Tienda')
	calle=models.CharField(max_length=64)
	altura=models.IntegerField()
	latitud=models.CharField(max_length=12)
	longitud=models.CharField(max_length=12)
	ciudad=models.ForeignKey('Ciudad')
	agendas = models.ManyToManyField(Agenda,blank=True)
	votos = models.ManyToManyField(Voto,blank=True)
	imagen = models.ImageField(upload_to="foto_localizaciones",blank=True)
	str_hash= models.CharField(max_length=12,blank=True,default='000000000000', editable=False)
	def __str__(self):
		return self.tienda.nombre +" "+ self.calle
	def __unicode__(self):
		return unicode(self.tienda.nombre) +" "+ unicode(self.calle)
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(39))
		super(Localizacion, self).save()
	def empaquetar(self):
		id_agendas=""
		for agenda in self.agendas.all():
			if id_agendas=="":
				id_agendas=str(agenda.id)
			else:
				id_agendas=id_agendas+","+str(agenda.id)
		id_votos=""
		for voto in self.votos.all():
			if id_votos=="":
				id_votos=str(voto.id)
			else:
				id_votos=id_votos+","+str(voto.id)
		try:
			img=self.imagen.url
		except:
			img=""
		rsp=str(self.id)+";"+str(self.tienda.id)+";"+self.calle+";"+str(self.altura)+";"+self.latitud+";"+self.longitud+";"+str(self.ciudad.id)+";"+id_agendas+";"+id_votos+";"+img+";"+self.str_hash
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	class Meta:
		verbose_name = "Comercio y/o dietética"
		verbose_name_plural = "Listado de comercios y dietéticas"

def post_save_localizacion(sender, instance, created, **kwargs):
	if instance.imagen:
		redimensionar_imagen(BASE_DIR+instance.imagen.url,400)

signals.post_save.connect(post_save_localizacion, sender=Localizacion)

class Producto(models.Model):
	marca = models.CharField(max_length=128)
	denominacion = models.CharField(max_length=512)
	rnpa=models.CharField(max_length=32)
	fecha_baja = models.DateField(null=True,blank=True)
	motivo_baja = models.CharField(max_length=256,null=True,blank=True)
	def empaquetar(self):
		rsp=str(self.id)+";"+unicode(self.marca)+";"+unicode(self.denominacion)+";"+str(self.rnpa)+";"+str(self.fecha_baja)+";"+unicode(self.motivo_baja)
		return rsp
	def __str__(self):
		return self.marca+" "+self.rnpa
	def __unicode__(self):
		return self.marca+" "+self.rnpa
	class Meta:
		verbose_name = "Producto"
		verbose_name_plural = "Listado de productos"

class Grupo(models.Model):
	tipo = models.CharField(max_length=256)
	productos = models.ManyToManyField(Producto,blank=True)
	str_hash= models.CharField(max_length=4,blank=True,default='0000', editable=False)
	def save(self):
		self.str_hash=str(StrongRandom().getrandbits(13))
		super(Grupo, self).save()
	def empaquetar(self):
		id_prod=""
		for producto in self.productos.all():
			if id_prod=="":
				id_prod=str(producto.id)
			else:
				id_prod=id_prod+","+str(producto.id)
		rsp=str(self.id)+";"+unicode(self.tipo)+";"+id_prod+";"+str(self.str_hash)
		return rsp
	def empaquetar_tipo(self):
		rsp=str(self.id)+";"+unicode(self.tipo)+";"+str(self.str_hash)
		return rsp
	def empaquetar_hash(self):
		rsp=str(self.id)+";"+self.str_hash
		return rsp
	def __str__(self):
		return self.tipo
	def __unicode__(self):
		return self.tipo
	class Meta:
		verbose_name = "Grupo de producto"
		verbose_name_plural = "Grupos asociados a los productos"

class Listado_Alimento(SingletonModel):
	fecha_actualizacion = models.DateField(null=True,blank=True)
	proxima_actualizacion = models.DateField(null=True,blank=True)
	grupos = models.ManyToManyField(Grupo,blank=True)
	def __str__(self):
		return "LISTADO INTEGRADO DE ALIMENTOS LIBRES DE GLUTEN - LEY 26.588"
	class Meta:
		verbose_name = "LISTADO INTEGRADO DE GRUPOS DE ALIMENTOS LIBRES DE GLUTEN"
