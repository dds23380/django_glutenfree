# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sessions.backends.file import SessionStore
from django.template.loader import get_template
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from Crypto.Random.random import StrongRandom
from django.shortcuts import render
from models import *
from funciones import *
from glutenfreeNOA.settings import *
from __init__ import rsa_manager

# Create your views here.
@csrf_exempt
def comunicate_with_post_method(request):
	respuesta="NO_POST"
	if request.method == 'POST':
		try:
			sessionid=request.POST.get("SESSIONID", "") #Si Android App no tiene sessionid envia NO_VALUE
		except:
			sessionid=""							
		#print "session received:"+str(sessionid)	
		if sessionid != "":					
			session = SessionStore(session_key=sessionid)
			request.session=session
			try:
				new=session['new']
			except:
				new=""
			if new=="":
				session['new']=1
			else:
				session['new']=0
				
		else:
			request.session.create()
			#print "Created session:"+str(request.session.session_key)
			session = request.session
			session['new']=1
		#print "Using session:"+str(request.session.session_key)
		try:
			consulta=request.POST.get("QUERY", "")
		except:
			consulta=None
		#### EMPIEZA CONSULTAS ####
		if consulta=="SYNCHRONIZE":			
			try:
				message=request.POST.get("PB", "")
			except:
				message=""
			if message != "":
				try:
					pubkey=request.session['pubkey']
				except:
					pubkey=""
				#print "pubkey actual: "+pubkey
				if pubkey != message:
					request.session['pubkey']=message
					pubkey=message
				try:
					msg_format="PUBKEY:"+str(rsa_manager.get_pubkey())
					respuesta=msg_format
				except:
					respuesta=None
				if respuesta==None:
					respuesta="ERROR"
			else:
				if sessionid==session.session_key:
					if session['new']==0:
						respuesta="SYNC_OK"
					else:
						respuesta="SESSIONID:"+str(session.session_key)
				else:
					respuesta="SESSIONID:"+str(session.session_key)
		elif consulta=="BD":
			key_new=""
			key=""
			key_update=""
			try:
				key=request.POST.get("GET", "")
			except:
				key=""
			if key == "GROUPS":
				tipo=""
				try:
					consulta2=request.POST.get("GROUPS", "")
					tipo="todo"
				except:
					consulta2=""
				if consulta2=="":
					try:
						consulta2=request.POST.get("TYPE", "")
						tipo="solo_tipo"
					except:
						consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
						except:
							consulta2=""
							respuesta="ERROR"
					for id_grupo in consulta2.split(","):
						try:
							grupo=Grupo.objects.get(pk=id_grupo)
						except:
							grupo=""
						if grupo!="":
							if tipo=="solo_tipo":
								rsp=grupo.empaquetar_tipo()
							elif tipo=="todo":
								rsp=grupo.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
					if respuesta=="":
						respuesta="ERROR"
				else:
					respuesta=""
					grupos=Grupo.objects.all()
					for grupo in grupos:
						rsp=grupo.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_GROUPS"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key == "PRODUCTS":
				tipo=""
				try:
					valor=request.POST.get("VALUE", "")
				except:
					valor=""
				try:
					tipo=request.POST.get("TYPE", "")
				except:
					tipo=""
				if valor != "" and tipo!="":
					respuesta=""
					if tipo=="0":
						#rnpa
						try:
							productos=Producto.objects.filter(rnpa__contains=valor)
						except:
							productos=None
						for producto in productos:
							grupoid=Grupo.objects.get(productos__id=producto.id).id
							rsp=producto.empaquetar()+";"+str(grupoid)
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
					elif tipo=="1":
						#marca
						try:
							productos=Producto.objects.filter(marca__contains=valor)
						except:
							productos=None
						for producto in productos:
							grupoid=Grupo.objects.get(productos__id=producto.id).id
							rsp=producto.empaquetar()+";"+str(grupoid)
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
					elif tipo=="2":
						#grupo
						try:
							grupo=Grupo.objects.get(tipo=valor)
						except:
							grupo=None
						for producto in grupo.productos.all():
							grupoid=grupo.id
							rsp=producto.empaquetar()+";"+str(grupoid)
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
					if respuesta!="":
						respuesta=comprimir_texto(respuesta)
						respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
					else:
						respuesta="NO_PRODUCTS"
				else:
					respuesta="NO_PRODUCTS"
			elif key == "PROFESSIONALS":
				tipo=""
				try:
					valor1=request.POST.get("VALUE1", "")
				except:
					valor1=""
				try:
					valor2=request.POST.get("VALUE2", "")
				except:
					valor2=""
				try:
					tipo=request.POST.get("TYPE", "")
				except:
					tipo=""
				if tipo!="":
					respuesta=""
					if tipo=="0":
						#matricula
						try:
							if valor1!="":
								medicos=Medico.objects.get(matricula=valor1)
							else:
								medicos=""
						except:
							medicos=""
						if medicos != "":
							for medico in medicos:
								rsp=medico.empaquetar()
								if respuesta=="":
									respuesta=rsp
								else:
									respuesta=respuesta+"\n"+rsp
					elif tipo=="1":
						#nombre
						try:
							if valor1!="":
								medicos=Medico.objects.filter(nombre__contains=valor1)
							else:
								medicos=""
						except:
							medicos=""
						if medicos!="":
							for medico in medicos:
								rsp=medico.empaquetar()
								if respuesta=="":
									respuesta=rsp
								else:
									respuesta=respuesta+"\n"+rsp
					elif tipo=="2":
						#especialidad
						try:
							if valor1!="" and valor2!="":
								mtipo=int(valor1)
								mespe=int(valor2)
								medicos=Medico.objects.filter(especialidad__tipo=valor1,especialidad__especialidad=valor2)
							else:
								medicos=""
						except:
							medicos=""
						if medicos!="":
							for medico in medicos:
								rsp=medico.empaquetar()
								if respuesta=="":
									respuesta=rsp
								else:
									respuesta=respuesta+"\n"+rsp
					if respuesta!="" and respuesta!="ERROR":
						respuesta=comprimir_texto(respuesta)
						respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
					elif respuesta=="":
						respuesta="NO_PROFESSIONALS"
				else:
					respuesta=""
					try:
						consulta2=request.POST.get("PROFESSIONALS", "")
					except:
						consulta2=""
					if consulta2!="":
						if consulta2.startswith("ZLIB:"):
							try:
								consulta2=descomprimir_texto(consulta2[5])
							except:
								consulta2=""
								respuesta="ERROR"
						for medico_id in consulta2.split(","):
							try:
								medico=Medico.objects.get(pk=medico_id)
							except:
								medico=""
							if medico!="":
								rsp=medico.empaquetar()
								if respuesta=="":
									respuesta=rsp
								else:
									respuesta=respuesta+"\n"+rsp
					else:
						medicos=Medico.objects.all()
						for medico in medicos:
							rsp=medico.id+";"+medico.get_hash()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
					if respuesta!="" and respuesta!="ERROR":
						respuesta=comprimir_texto(respuesta.encode('utf-8'))
						respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
					elif respuesta=="":
						respuesta="NO_PROFESSIONALS"
			elif key == "CITYS":
				respuesta=""
				ciudades=Ciudad.objects.all()
				for ciudad in ciudades:
					rsp=ciudad.empaquetar()
					if respuesta=="":
						respuesta=rsp
					else:
						respuesta=respuesta+"\n"+rsp
				if respuesta!="":
					respuesta="ZLIB:"+base64.standard_b64encode(comprimir_texto(respuesta))
				else:
					respuesta="NO_CITYS"
			elif key == "LOCATIONS":
				try:
					consulta2=request.POST.get("LOCATIONS", "")
				except:
					consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
							
						except:
							consulta2=""
							respuesta="ERROR"
					for id_comercio in consulta2.split(","):
						try:
							comercio=Localizacion.objects.get(pk=id_comercio)
						except:
							comercio=""
						if comercio!="":
							rsp=comercio.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
				else:
					respuesta=""
					comercios=Localizacion.objects.all()
					for comercio in comercios:
						rsp=comercio.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_LOCATIONS"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key == "VOTE_LOCATIONS":
				try:
					consulta2=request.POST.get("LOCATIONS", "")
				except:
					consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
							
						except:
							consulta2=""
							respuesta="ERROR"
					for id_comercio in consulta2.split(","):
						try:
							comercio=Localizacion.objects.get(pk=id_comercio)
						except:
							comercio=""
						if comercio!="":
							rsp=comercio.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
				else:
					respuesta=""
					comercios=Localizacion.objects.all()
					for comercio in comercios:
						rsp=comercio.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_LOCATIONS"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key == "USERS":
				respuesta=""
				try:
					pubkey=request.session['pubkey']
				except:
					pubkey=""
				if pubkey!="":
					try:
						consulta2=request.POST.get("USERID", "")
						usuario = Usuario.objects.get(pk=int(consulta2))
					except:
						usuario=None
					if usuario!=None:
						respuesta=usuario.empaquetar()
					else:
						try:
							consulta2=request.POST.get("USER", "")
							usuario = Usuario.objects.get(usuario=consulta2)
						except:
							usuario=None
						if usuario!=None:
							try:
								consulta3=request.POST.get("PW", "")
							except:
								consulta3=""
							try:
								if consulta3!="":
									pw=rsa_manager.decrypt_message(consulta3)
								else:
									pw=""
							except:
								pw=""
							if usuario.contrasena==pw:
								respuesta="OK "+str(usuario.id)
							else:
								respuesta="WRONG_USER_OR_PASSWORD"
						else:
							respuesta="WRONG_USER_OR_PASSWORD"
				else:
					respuesta="ERROR"
				#print "rsp: "+respuesta
			elif key == "VOTES":
				respuesta=""
				try:
					loc_id=request.POST.get("LOC_ID", "")
				except:
					loc_id=""
				try:
					user_id=request.POST.get("USER_ID", "")
				except:
					user_id=""
				try:
					localizacion = Localizacion.objects.get(pk=loc_id)
				except:
					localizacion = None
				if localizacion != None:
					if user_id=="":
						votos=localizacion.votos.all()
					else:
						votos=localizacion.votos.filter(usuario__pk=user_id)
					for voto in votos:
						rsp=voto.empaquetar()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta!="":
					respuesta="ZLIB:"+base64.standard_b64encode(comprimir_texto(respuesta))
				else:
					respuesta="NO_VOTE"
			elif key == "AGENDAS":
				try:
					consulta2=request.POST.get("AGENDAS", "")
				except:
					consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
						except:
							consulta2=""
							respuesta="ERROR"
					for id_agenda in consulta2.split(","):
						try:
							agenda=Agenda.objects.get(pk=id_agenda)
						except:
							agenda=""
						if agenda!="":
							rsp=agenda.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
				else:
					respuesta=""
					agendas=Agenda.objects.all()
					for agenda in agendas:
						rsp=agenda.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_AGENDAS"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key == "TENDERS":
				respuesta=""
				tiendas = Tienda.objects.all()
				for tienda in tiendas:
					rsp=tienda.empaquetar()
					if respuesta=="":
						respuesta=rsp
					else:
						respuesta=respuesta+"\n"+rsp
				if respuesta!="":
					respuesta="ZLIB:"+base64.standard_b64encode(comprimir_texto(respuesta))
				else:
					respuesta="NO_TENDERS"
			elif key == "INFORMATION":
				try:
					consulta2=request.POST.get("INFORMATION", "")
				except:
					consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
						except:
							consulta2=""
							respuesta="ERROR"
					for id_info in consulta2.split(","):
						try:
							info=Informacion.objects.get(pk=id_info)
						except:
							info=""
						if info!="":
							rsp=info.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
				else:
					respuesta=""
					infos=Informacion.objects.all()
					for info in infos:
						rsp=info.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_INFORMATION"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key == "CONSULTORIOS":
				try:
					consulta2=request.POST.get("CONSULTORIOS", "")
				except:
					consulta2=""
				if consulta2 != "":
					respuesta=""
					if consulta2.startswith("ZLIB:"):
						try:
							consulta2=descomprimir_texto(consulta2[5])
						except:
							consulta2=""
							respuesta="ERROR"
					for id_atencion in consulta2.split(","):
						try:
							atencion=Atencion.objects.get(pk=id_atencion)
						except:
							atencion=""
						if atencion!="":
							rsp=atencion.empaquetar()
							if respuesta=="":
								respuesta=rsp
							else:
								respuesta=respuesta+"\n"+rsp
				else:
					respuesta=""
					atenciones=Atencion.objects.all()
					for atencion in atenciones:
						rsp=atencion.empaquetar_hash()
						if respuesta=="":
							respuesta=rsp
						else:
							respuesta=respuesta+"\n"+rsp
				if respuesta=="":
					respuesta="NO_CONSULTORIOS"
				elif respuesta!="ERROR":
					respuesta=comprimir_texto(respuesta)
					respuesta="ZLIB:"+base64.standard_b64encode(respuesta)
			elif key!="":
				respuesta="UNKNOWN_KEY"
			else:
				try:
					key_new=request.POST.get("NEW", "")
				except:
					key_new=""
			if key_new=="VOTE":
				#ID USERID USER VOTE COMMENT
				comercio_id=""
				try:
					user_id=request.POST.get("USERID", "")
					muser=Usuario.objects.get(pk=user_id)
					try:
						user=request.POST.get("USER", "")
					except:
						user=""
					if user!="" and muser.usuario==user:
						logued=True
					else:
						logued=None
				except:
					logued=None
				if logued==True:
					try:
						comercio_id=request.POST.get("ID", "")
						mcomercio=Localizacion.objects.get(pk=comercio_id)
					except:
						mcomercio=None
					if mcomercio!=None:
						try:
							vote=int(request.POST.get("VOTE", ""))
						except:
							vote=2
						try:
							comment=request.POST.get("COMMENT", "")
						except:
							comment=""
						try:
							nvoto=Voto(voto=vote,usuario=muser,comentario=comment)
							nvoto.save()
							mcomercio.votos.add(nvoto)
							mcomercio.save()
							respuesta="NEW_VOTE_OK"
						except:
							respuesta="ERROR"
					else:
						respuesta="WRONG_COMERCIO"
				else:
					respuesta="WRONG_USER"
			if key_new=="USER":
				#USER PW NAME EMAIL PHONE MOVIL IMAGE
				try:
					user=request.POST.get("USER", "")
				except:
					user=""
				try:
					pw=rsa_manager.decrypt_message(request.POST.get("PW", ""))
				except:
					pw=""
				try:
					nombre=request.POST.get("NAME", "")
				except:
					nombre=""
				try:
					email=request.POST.get("EMAIL", "")
				except:
					email=""
				try:
					phone=request.POST.get("PHONE", "")
				except:
					phone=""
				try:
					movil=request.POST.get("MOVIL", "")
				except:
					movil=""
				try:
					image=request.POST.get("IMAGE", "")
				except:
					image=""
				if user == "" or pw == "" or nombre == "" or email=="":
					respuesta="NEW_USER_ERROR"
				else:
					try:
						try:
							tmp_user=Usuario.objects.get(usuario=user)
						except:
							tmp_user=""
						if tmp_user!= "":
							respuesta="NEW_USER_EXIST"
						else: 
							try:
								tmp_ag=Agenda.objects.filter(email=email)
							except:
								tmp_ag=""
							if len(tmp_ag) > 0:
								respuesta="EMAIL_EXIST"
							else:
								try:
									image_decoded = base64.standard_b64decode(image)
								except:
									image_decoded=""
								nagenda=Agenda(nombre=nombre,email=email,fijo=phone,celular=movil)
								if image_decoded != "":
									try:
										filename=str(StrongRandom().getrandbits(39))+".png"
										with open("media/foto_perfiles/"+filename, 'wb') as f:
											f.write(image_decoded)
										nagenda.foto_perfil="foto_perfiles/"+filename
									except:
										nagenda.foto_perfil=""
								nagenda.save()
								nuser=Usuario(usuario=user,contrasena=pw,agenda=nagenda)
								nuser.save()
								respuesta="NEW_USER_OK"
					except:
						respuesta="ERROR"
			elif key_new != "":
				respuesta="UNKNOWN_KEY"
			else:
				try:
					key_update=request.POST.get("UPDATE", "")
				except:
					key_update=""
				if key_update=="VOTE":
					try:
						user_id=request.POST.get("USERID", "")
						muser=Usuario.objects.get(pk=user_id)
						try:
							user=request.POST.get("USER", "")
						except:
							user=""
						if user!="" and muser.usuario==user:
							logued=True
						else:
							logued=None
					except:
						logued=None
					if logued==True:
						try:
							voto_id=request.POST.get("ID", "")
							voto=Voto.objects.get(pk=voto_id)
						except:
							voto_id=None
						if voto_id!=None:
							try:
								vote=int(request.POST.get("VOTE", ""))
							except:
								vote=2
							try:
								comment=request.POST.get("COMMENT", "")
							except:
								comment=""
							try:
								voto.voto=vote
								voto.comentario=comment
								voto.save()
								respuesta="UPDATE_VOTE_OK"
							except:
								respuesta="ERROR"
						else:
							respuesta="WRONG_VOTE"
					else:
						respuesta="WRONG_USER"
				elif key_update != "":
					respuesta="UNKNOWN_KEY"
		elif consulta=="PING":
			respuesta="IS_OK"
		else:
			respuesta="UNKNOWN_QUERY"
		request.session.save()
	#print "Respondiendo: "+respuesta
	return HttpResponse(respuesta)
