# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movil_server', '0002_auto_20150828_1441'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comentario_precios',
            name='usuario',
        ),
        migrations.AlterModelOptions(
            name='listado_alimento',
            options={'verbose_name': 'LISTADO INTEGRADO DE GRUPOS DE ALIMENTOS LIBRES DE GLUTEN'},
        ),
        migrations.DeleteModel(
            name='Comentario_Precios',
        ),
    ]
