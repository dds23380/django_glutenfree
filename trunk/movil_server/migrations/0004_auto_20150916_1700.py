# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movil_server', '0003_auto_20150828_1713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agenda',
            name='email',
            field=models.CharField(default='NOEMAIL', max_length=64),
        ),
    ]
