# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agenda',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=64)),
                ('email', models.CharField(max_length=64, blank=True)),
                ('fijo', models.CharField(max_length=64, blank=True)),
                ('celular', models.CharField(max_length=64, blank=True)),
                ('foto_perfil', models.ImageField(upload_to='foto_perfiles', blank=True)),
                ('str_hash', models.CharField(default='000000000000', max_length=12, editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Atencion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=64)),
                ('domicilio', models.CharField(max_length=64)),
                ('latitud', models.CharField(max_length=12, blank=True)),
                ('longitud', models.CharField(max_length=12, blank=True)),
                ('horario', models.CharField(max_length=64, blank=True)),
                ('str_hash', models.CharField(default='000000000000', max_length=12, editable=False, blank=True)),
                ('agenda', models.ManyToManyField(to='movil_server.Agenda', blank=True)),
            ],
            options={
                'verbose_name': 'Consultorio',
            },
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ciudad', models.CharField(max_length=64)),
                ('provincia', models.CharField(max_length=64)),
                ('pais', models.CharField(max_length=64)),
            ],
            options={
                'verbose_name': 'Ciudade',
            },
        ),
        migrations.CreateModel(
            name='Especialidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.IntegerField(default=1, choices=[(1, 'MEDICO'), (2, 'KINESIOLOGIA'), (3, 'ENFERMERIA'), (4, 'FONOAUDIOLIGIA'), (5, 'NUTRICION')])),
                ('especialidad', models.IntegerField(default=20, choices=[(1, 'ALERGOLOGIA'), (2, 'ANATOMIA PATOLOGICA'), (3, 'ANESTESIOLOGIA'), (4, 'BACTERIOLOGIA MEDICA'), (5, 'CARDIOLOGIA'), (6, 'CARDIOLOGIA INFANTIL'), (7, 'CIRUGIA CARDIOVASCULAR'), (8, 'CIRUGIA DE CABEZA Y CUELLO'), (9, 'CIRUGIA DE TORAX'), (10, 'CIRUGIA GENERAL'), (11, 'CIRUGIA PEDIATRICA'), (12, 'CIRUGIA PLASTICA'), (13, 'CLINICA MEDICA'), (14, 'DERMATOLOGIA'), (15, 'DIAGNOSTICO POR IMAGENES'), (16, 'ECOGRAFIA'), (17, 'ENDOCRINOLOGIA'), (18, 'FISIATRIA'), (19, 'FLEBOLOGIA Y LINFOLOGIA'), (20, 'GASTROENTEROLOGIA'), (21, 'GENETICA'), (22, 'GERIATRIA'), (23, 'GINECOLOGIA'), (24, 'HEBIATRIA'), (25, 'HEMATO-ONCOLOGIA PEDIATRICA'), (26, 'HEMATOLOGIA'), (27, 'INFECTOLOGIA'), (28, 'MEDICINA DE FAMILIA'), (29, 'MEDICINA DEL DEPORTE'), (30, 'MEDICINA DEL TRABAJO'), (31, 'MEDICINA GENERAL'), (32, 'MEDICINA NUCLEAR'), (33, 'MEDICO'), (34, 'MEDICO GENERALISTA'), (35, 'Muchas Especialidades'), (36, 'NEFROLOGIA'), (37, 'NEONATOLOGIA'), (38, 'NEUMONOLOGIA'), (39, 'NEUMONOLOGIA INFANTIL'), (40, 'NEUROCIRUGIA'), (41, 'NEUROLOGIA'), (42, 'NEUROLOGIA INFANTIL'), (43, 'NUTRICION'), (44, 'OFTALMOLOGIA'), (45, 'ONCOLOGIA'), (46, 'ORTOPEDIA Y TRAUMATOLOGIA'), (47, 'ORTOPEDIA Y TRAUMATOLOGIA INFANTIL'), (48, 'OTORRINOLARINGOLOGIA'), (49, 'PEDIATRIA'), (50, 'PSIQUIATRIA'), (51, 'RADIOLOGIA'), (52, 'REUMATOLOGIA'), (53, 'TERAPIA INTENSIVA'), (54, 'TOCOGINECOLOGIA'), (55, 'UROLOGIA'), (56, 'LICENCIATURA')])),
            ],
            options={
                'verbose_name': 'Especialidade',
            },
        ),
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=256)),
                ('str_hash', models.CharField(default='0000', max_length=4, editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Listado_Alimento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fecha_actualizacion', models.DateField(null=True, blank=True)),
                ('proxima_actualizacion', models.DateField(null=True, blank=True)),
                ('grupos', models.ManyToManyField(to='movil_server.Grupo', blank=True)),
            ],
            options={
                'verbose_name': 'LISTADO INTEGRADO DE ALIMENTOS LIBRES DE GLUTEN',
            },
        ),
        migrations.CreateModel(
            name='Localizacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('calle', models.CharField(max_length=64)),
                ('altura', models.IntegerField()),
                ('latitud', models.CharField(max_length=12)),
                ('longitud', models.CharField(max_length=12)),
                ('imagen', models.ImageField(upload_to='foto_localizaciones', blank=True)),
                ('str_hash', models.CharField(default='000000000000', max_length=12, editable=False, blank=True)),
                ('agendas', models.ManyToManyField(to='movil_server.Agenda', blank=True)),
                ('ciudad', models.ForeignKey(to='movil_server.Ciudad')),
            ],
            options={
                'verbose_name': 'Listado de comercios y diet\xe9tica',
            },
        ),
        migrations.CreateModel(
            name='Medico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=64)),
                ('matricula', models.CharField(max_length=64, blank=True)),
                ('foto_perfil', models.ImageField(upload_to='foto_perfiles_medicos', blank=True)),
                ('str_hash', models.CharField(default='000000000000', max_length=12, editable=False, blank=True)),
                ('atencion', models.ManyToManyField(to='movil_server.Atencion', blank=True)),
                ('especialidad', models.ManyToManyField(to='movil_server.Especialidad')),
            ],
            options={
                'verbose_name': 'Listado de M\xe9dico',
            },
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('marca', models.CharField(max_length=128)),
                ('denominacion', models.CharField(max_length=512)),
                ('rnpa', models.CharField(max_length=32)),
                ('fecha_baja', models.DateField(null=True, blank=True)),
                ('motivo_baja', models.CharField(max_length=256, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tienda',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=64)),
                ('tipo', models.IntegerField(default=1, choices=[(1, 'Diet\xe9tica'), (2, 'Grandes tiendas'), (3, 'Restaurante & Bar')])),
            ],
            options={
                'verbose_name': 'Definici\xf3n de tienda',
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('usuario', models.CharField(max_length=64)),
                ('contrasena', models.CharField(max_length=64, blank=True)),
                ('permisos', models.IntegerField(default=1000)),
                ('agenda', models.ForeignKey(to='movil_server.Agenda')),
            ],
        ),
        migrations.CreateModel(
            name='Voto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('voto', models.IntegerField()),
                ('comentario', models.TextField(max_length=128, blank=True)),
                ('usuario', models.ForeignKey(to='movil_server.Usuario')),
            ],
        ),
        migrations.AddField(
            model_name='localizacion',
            name='tienda',
            field=models.ForeignKey(to='movil_server.Tienda'),
        ),
        migrations.AddField(
            model_name='localizacion',
            name='votos',
            field=models.ManyToManyField(to='movil_server.Voto', blank=True),
        ),
        migrations.AddField(
            model_name='grupo',
            name='productos',
            field=models.ManyToManyField(to='movil_server.Producto', blank=True),
        ),
        migrations.AddField(
            model_name='atencion',
            name='ciudad',
            field=models.ForeignKey(to='movil_server.Ciudad'),
        ),
    ]
