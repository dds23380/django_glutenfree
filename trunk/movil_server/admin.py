from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import *

# Register your models here.

admin.site.register(Usuario)
class AgendaAdmin(admin.ModelAdmin):
	list_display = ('nombre','str_hash','foto_perfil')
	fields = ['nombre','email','fijo','celular','foto_perfil']
admin.site.register(Agenda,AgendaAdmin)
class LocalizacionAdmin(admin.ModelAdmin):
	list_display = ('tienda','calle','altura','ciudad','str_hash')
	fields = ['tienda','calle','altura','latitud','longitud','ciudad','agendas','imagen']
admin.site.register(Localizacion,LocalizacionAdmin)
class TiendaAdmin(admin.ModelAdmin):
	list_display = ('nombre','tipo')
admin.site.register(Tienda,TiendaAdmin)
admin.site.register(Ciudad)
admin.site.register(Listado_Alimento, SingletonModelAdmin)
listado_alimentos = Listado_Alimento.get_solo()
class GrupoAdmin(admin.ModelAdmin):
	list_display = ('tipo','str_hash')
	fields = ['tipo','productos']
admin.site.register(Grupo,GrupoAdmin)
class ProductosAdmin(admin.ModelAdmin):
	list_display = ('marca','denominacion','rnpa')
	fields = ['marca','denominacion','rnpa']
admin.site.register(Producto,ProductosAdmin)
class MedicoAdmin(admin.ModelAdmin):
	list_display = ('nombre','matricula','str_hash')
	fields = ['nombre','especialidad','matricula','foto_perfil','atencion']
admin.site.register(Medico,MedicoAdmin)
admin.site.register(Especialidad)
class AtencionAdmin(admin.ModelAdmin):
	fields = ['tipo','domicilio','ciudad','latitud','longitud','horario','agenda']
admin.site.register(Atencion,AtencionAdmin)
admin.site.register(Voto)
