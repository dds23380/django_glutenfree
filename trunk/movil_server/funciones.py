# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from Crypto.PublicKey import RSA
from glutenfreeNOA.settings import *
import zlib
import os
import base64
import PIL
from PIL import Image

class RSA_Manager:
	def __init__(self,bits):
		#self.open_private_file(prvfilename)
		#self.open_public_file(pubfilename)
		self.generate_pair(bits)		
	def generate_pair(self,bits):
		self.prvkey = RSA.generate(bits)
		self.pubkey = self.prvkey.publickey()
	def open_private_file(self,prvfilename):
		try:
			prvfile=open(prvfilename)
			self.prvkey=RSA.importKey(prvfile.read())
			prvfile.close()
		except:
			self.prvkey=None
	def open_public_file(self,pubfilename):
		try:
			pubfile=open(pubfilename)
			self.pubkey=RSA.importKey(pubfile.read())
			pubfile.close()
		except:
			self.pubkey=None
	def open_ext_public_file(self,pubkey):
		try:
			self.ext_pubkey=RSA.importKey(pubkey)
		except:
			self.ext_pubkey=None
	def encrypt_message(self,message):
		if self.pubkey:
			try:
				if self.pubkey.can_encrypt():
					msg=self.pubkey.encrypt(message,0)
					return base64.standard_b64encode(msg[0])
				else: 
					return None
			except:
				return None
		return None
	def encrypt_message_with_ext_pubkey(self,ext_pubkey,message):
		try:
			pubkey=RSA.importKey(base64.b64decode(ext_pubkey))
		except:
			print "Error al importar pubkey"
			pubkey=None
		if pubkey:
			if pubkey.can_encrypt():
				try:
					msg=pubkey.encrypt(str(message),None)
				except:
					return None
				return base64.standard_b64encode(msg[0])
			else: 
				return None
		else: 
			return None
	def decrypt_message(self,message):
		if self.prvkey:
			try:
				msg=self.prvkey.decrypt(base64.decodestring(message))
				return msg
			except:
				return None
		return None
	def get_pubkey(self):
		return base64.standard_b64encode(self.pubkey.exportKey(format="DER"))

def clear_session_dir():
	for root, dirs, files in os.walk(BASE_DIR+"/sessions", topdown=False):
		for name in files:
			os.remove(os.path.join(root, name))

def comprimir_texto(texto):
	try:
		return zlib.compress(texto)
	except:
		try:
			return zlib.compress(texto.encode('utf-8'))
		except:
			try:
				return texto.encode('utf-8').encode('zlib_codec')
			except:
				return ""

def descomprimir_texto(texto):
	try:
		return zlib.decompress(texto)
	except:
		try:
			return texto.decode('zlib_codec').decode('utf-8')
		except:
			return ""

def redimensionar_imagen(imagen, ancho_base):
	if imagen=="":
		return
	try:
		img = Image.open(imagen)
	except:
		print "Imagen "+imagen+" no existe."
		return
	if img.size[0]<ancho_base:
		return
	try:
		wpercent = (ancho_base/float(img.size[0]))
		hsize = int((float(img.size[1])*float(wpercent)))
		img = img.resize((ancho_base,hsize), PIL.Image.ANTIALIAS)
		img.save(imagen)
	except:
		print "Error al dimensionar imagen."
