#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from blog.models import *
from parseador import *
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

# Create your views here.
def index(request):
	if request.method == 'POST':
		try:
			archivo=request.FILES['archivo']
		except:
			return render(request, 'parseador/index.html', {'respuesta': "Debe seleccionar un archivo válido."})
		# parse command line options
		try:
			template=archivo.read().split("\n")
		except:
			return render(request, 'parseador/index.html', {'respuesta': "ERR: Al intentar abrir "+ archivo.name})
		grupos,pgroups=load_groups_from_template(template)
		if pgroups=="" or grupos=="": 
			return render(request, 'parseador/index.html', {'respuesta': "ERR: Al intentar procesar template."})
		proceso=request.POST.get('proceso')
		if proceso=="0":
			return render(request, 'parseador/index.html', {'pgrupos':pgroups,'grupos':grupos,'respuesta':''})
		elif proceso=="1":
			actualizar_grupos_bd(grupos)
			actualizar_productos_bd(grupos,pgroups)
			return render(request, 'parseador/index.html', {'respuesta':'Base de datos cargada satisfactoria'})
	return render(request, 'parseador/index.html',{'respuesta':''})
