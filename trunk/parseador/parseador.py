#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import os
from blog.models import *
from movil_server.models import *
from movil_server.admin import listado_alimentos

patron=[]
patron.append('^\[([ Á-ÚA-ZÑ,\(\)\-0-9]*)?\]$')
patron.append('^ *([ Á-ÚA-ZÑ,\(\)\-0-9]*)? * $')
patron.append('^.?MARCA.*NOMBRE.*DENOMINACION *RNPA *$')
patron.append('^([\(\),a-zA-Z\–\-\'0-9\.\á-\ú\Á-\Ú\ñ\Ñ\%;\°\® ]*)? ([I0-9\,\/\-.]*)?$')

def load_groups_list_from_template(template):
	if template == "":
		return ""
	grupos=[]
	seccion=""
	archivo=""
	prog = re.compile(patron[0])
	for fila in template:
		try:
			rlt=prog.match(fila).group(1)
		except:
			rlt=None
		if rlt!=None:
			seccion=rlt
			continue
		if seccion=="GRUPOS":
			if fila!="":
				grupos.append(fila)
		elif seccion=="GENERAL":
			if fila[0:5]=="FILE=":
				archivo=fila[5:]
	return grupos,archivo

def load_groups_from_template(template):
	grupos,archivo=load_groups_list_from_template(template)
	if grupos=="": 
		return ""
	grupos_filas=[]
	for i in range(len(grupos)):
		grupos_filas.insert(i,[])
	header_grupo = re.compile(patron[1])
	grupo_actual=""
	try:
		txtfile=open(archivo,"r").read().split("\n")
	except:
		return grupos,""
	for linea in txtfile:
		rlt=""
		try:
			rlt=header_grupo.match(linea).group(1)
			rlt=rlt.rstrip()
			try:
				valor=grupos.index(rlt)
				grupo_actual=valor
			except:
				grupo_actual=grupo_actual
		except:
			rlt=None
		if rlt==None:
			if linea!="":
				grupos_filas[grupo_actual].append(linea)
	pgrupos=process_rows_from_groups(grupos_filas)
	return grupos,pgrupos

def process_rows_from_groups(fgrupos):
	pgrupos=[]
	header_col = re.compile(patron[2])
	fixed=[]
	fixed.append(0)
	fixed.append(0)
	header_file = re.compile(patron[3])
	mgrupo=""
	for grupo in fgrupos:
		fixed[0]=0
		fixed[1]=0
		mgrupo=fgrupos.index(grupo)
		pgrupos.insert(mgrupo,[])
		for fila in grupo:
			try:
				rlt=header_col.match(fila).group(0)
				#matcheo
				valor=fila.find('DENOMINACION')
				fixed[0]=valor
				valor=fila.find('RNPA')
				fixed[1]=valor
			except:
				#no matcheo
				if fixed[0] == 0 or fixed[1] == 0:
					continue
				try:
					rlt=[find_nonascii_chars(fila,0,fixed[0]-1),find_nonascii_chars(fila,fixed[0],len(fila))]
				except:
					rlt=None
				if rlt!=None and len(rlt)==2:
					if len_nonascii_chars(fila)<fixed[1]-1:
						dato_a=rlt[0]
						dato_b=rlt[1]
						dato_c=""
					else:
						dato_a=rlt[0]
						try:
							rlt2=header_file.match(rlt[1]).groups()
							dato_b=rlt2[0]
							dato_c=rlt2[1]
						except:
							dato_b=""
							dato_c=""
					if dato_a!=None:
						dato_a=dato_a.strip()
					else:
						dato_a=""
					if dato_b!=None:
						dato_b=dato_b.strip()
					else:
						dato_b=""
					if dato_c!=None:
						dato_c=dato_c.strip()
					else:
						dato_c=""
					if dato_a=="" or dato_b=="" or dato_c=="":
						anterior=len(pgrupos[mgrupo])-1
						if anterior>=0:
							pgrupos[mgrupo][anterior][0]=pgrupos[mgrupo][anterior][0]+" "+dato_a
							pgrupos[mgrupo][anterior][1]=pgrupos[mgrupo][anterior][1]+" "+dato_b
							pgrupos[mgrupo][anterior][2]=pgrupos[mgrupo][anterior][2]+dato_c
					else:
						pgrupos[mgrupo].append([dato_a,dato_b,dato_c])
	return pgrupos

def find_nonascii_chars(mstr,init,finish):
	i=0
	cext=False
	cstr=""
	for c in mstr:
		if i>=finish:
			break;
		if ord(c) < 128:
			i=i+1
		else:
			if cext==False:
				cext=True
			else: 
				cext=False
				i=i+1
		if i>=init:
			cstr=cstr+c
	return cstr

def len_nonascii_chars(mstr):
	i=0
	cext=False
	for c in mstr:
		if ord(c) < 128:
			i=i+1
		else:
			if cext==False:
				cext=True
			else: 
				cext=False
				i=i+1
	return i

def actualizar_grupos_bd(grupos):
	for grupo in grupos:
		try:
			listado_alimentos.grupos.get(tipo=grupo)
		except:
			grp=Grupo(tipo=grupo)
			grp.save()
			listado_alimentos.grupos.add(grp)
	listado_alimentos.save()

def actualizar_productos_bd(grupos,pgrupos):
	for index, grp in enumerate(pgrupos, start=0):
		try:
			grupo=grupos[index]
			grupo_bd=listado_alimentos.grupos.get(tipo=grupo)
		except:
			grupo_bd=None
		if grupo_bd!=None:
			for producto in grp:
				marca=producto[0]
				denominacion=producto[1]
				rnpa=producto[2]
				try:
					newprod=grupo_bd.productos.get(rnpa=rnpa)
					newprod.marca=marca
					newprod.denominacion=denominacion
					newprod.save()
				except:
					newprod=Producto(marca=marca,denominacion=denominacion,rnpa=rnpa)
					newprod.save()
					grupo_bd.productos.add(newprod)
			grupo_bd.save()
	listado_alimentos.save()

